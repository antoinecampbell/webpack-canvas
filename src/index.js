import Ship from "./ship.js";
import './style.less';

document.addEventListener('DOMContentLoaded', event => {

  const app = document.getElementById('app');
  app.innerHTML = `
    <h1>Canvas</h1>
    <p>Some normal text</p>
    <div class="canvas-container">
        <canvas id="canvas" width="800" height="500"/>
     </div>`;
  const canvas = document.getElementById('canvas');
  const context = canvas.getContext('2d');
  context.globalCompositeOperation = 'lighter';

  const ship1 = new Ship(50, 50);
  let lastRender = 0;
  let loopActive;

  const loop = timestamp => {
    const gameTime = timestamp - lastRender;
    context.clearRect(0, 0, canvas.width, canvas.height);

    ship1.update(gameTime);
    ship1.draw(context);

    if (loopActive) {
      window.requestAnimationFrame(loop);
    }
    lastRender = timestamp;
  };

  const btn = document.createElement('button');
  btn.innerHTML = 'Toggle loop';
  btn.onclick = () => {
    if (!loopActive) {
      window.requestAnimationFrame(loop);
      canvas.classList.add('active');
    } else {
      canvas.classList.remove('active');
    }
    loopActive = !loopActive;
  };
  document.querySelector('body').appendChild(btn);

});
