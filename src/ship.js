import {EngineParticles} from "./engine-particle";

export default class Ship {

  constructor(positionX, positionY) {
    this.positionX = positionX;
    this.positionY = positionY;
    this.velocityX = 0;
    this.velocityY = 0;
    this.rotation = 0;
    this.engineParticles = new EngineParticles();
    this.keys = {
      up: false,
      down: false,
      left: false,
      right: false,
      space: false
    };

    document.addEventListener('keydown', event => {
      switch (event.key) {
        case 'ArrowRight':
          this.keys.right = true;
          break;
        case 'ArrowLeft':
          this.keys.left = true;
          break;
        case 'ArrowUp':
          this.keys.up = true;
          break;
        case 'ArrowDown':
          this.keys.down = true;
          break;
        case ' ':
          this.keys.space = true;
          break;
      }
    });

    document.addEventListener('keyup', event => {
      switch (event.key) {
        case 'ArrowRight':
          this.keys.right = false;
          break;
        case 'ArrowLeft':
          this.keys.left = false;
          break;
        case 'ArrowUp':
          this.keys.up = false;
          break;
        case 'ArrowDown':
          this.keys.down = false;
          break;
        case ' ':
          this.keys.space = false;
          break;
      }
    });
  }

  update(gameTime) {
    if (this.keys.up) {
      const ax = Math.cos(this.rotation * Math.PI / 180);
      const ay = Math.sin(this.rotation * Math.PI / 180);
      this.velocityX += ax;
      this.velocityY += ay;
      this.engineParticles.addParticles(this.positionX, this.positionY, this.rotation,
        this.velocityX, this.velocityY);
    } else if (this.keys.down) {
      this.velocityX -= 2 * Math.cos(this.rotation * Math.PI / 180);
      this.velocityY -= 2 * Math.sin(this.rotation * Math.PI / 180);
    }

    if (this.keys.left) {
      this.rotation -= 45 / gameTime;
    } else if (this.keys.right) {
      this.rotation += 45 / gameTime;
    }

    if (this.keys.space) {
      this.velocityX *= 0.95;
      this.velocityY *= 0.95;
    }

    // TODO: clamp velocity magnitude
    this.positionX += this.velocityX / gameTime;
    this.positionY += this.velocityY / gameTime;

    this.engineParticles.update(gameTime);
  }

  draw(context, color = 'red', size = 25) {
    this.engineParticles.draw(context);

    context.save();

    context.strokeStyle = color;

    context.beginPath();
    context.arc(this.positionX, this.positionY, 2, 0, 2 * Math.PI);
    context.stroke();

    context.translate(this.positionX, this.positionY);
    context.rotate(this.rotation * Math.PI / 180);
    context.translate(-size / 2, -size / 2);

    context.beginPath();
    context.moveTo(0, 0);
    context.lineTo(size, size / 2);
    context.lineTo(0, size);
    context.lineTo(size / 3, size / 2);
    context.lineTo(0, 0);
    context.stroke();

    context.restore();
  }

}
