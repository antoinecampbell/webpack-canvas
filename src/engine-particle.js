export class EngineParticles {

  constructor(particleLimit = 50) {
    this.particles = [];
    for (let i = 0; i < particleLimit; i++) {
      this.particles.push(new Particle());
    }
  }

  addParticles(x, y, rotation, sx, sy, count = 1) {
    const availableParticles = this.particles.filter(particle => !particle.ttl);
    if (availableParticles.length > count) {
      for (let i = 0; i < count; i++) {
        const randomRotation = rotation + this.getRandomInt(-15, 15);
        const randomVelocity = this.getRandomInt(-40, -30);
        const asx = Math.abs(sx);
        const asy = Math.abs(sy);
        const ax = randomVelocity * Math.cos(randomRotation * Math.PI / 180);
        const ay = randomVelocity * Math.sin(randomRotation * Math.PI / 180);
        availableParticles[i].setParticle(x, y, ax, ay);
      }
    }
  }

  update(gameTime) {
    this.particles.forEach(particle => {
      if (particle.ttl) {
        particle.update(gameTime);
      }
    });
  }

  draw(context) {
    this.particles.forEach(particle => {
      if (particle.ttl) {
        particle.draw(context);
      }
    });
  }

  getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min;
  }
}

export class Particle {

  constructor(x = 0, y = 0, vx = 0, vy = 0) {
    this.positionX = x;
    this.positionY = y;
    this.velocityX = vx;
    this.velocityY = vy;
    this.radius = 3;
    this.color = 'orange';
    this.ttl = 0;
  }

  setParticle(x, y, vx, vy) {
    this.positionX = x;
    this.positionY = y;
    this.velocityX = vx;
    this.velocityY = vy;
    this.ttl = 250;
  }

  update(gameTime) {
    this.ttl = Math.max(0, this.ttl - gameTime);
    this.positionX += this.velocityX / gameTime;
    this.positionY += this.velocityY / gameTime;
  }

  draw(context) {
    context.beginPath();
    context.arc(this.positionX, this.positionY, this.radius, 0, 2 * Math.PI);
    context.fillStyle = this.color;
    context.fill();
  }
}
